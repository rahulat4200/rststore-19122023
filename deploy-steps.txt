cd frontend/
Remove-item -Recurse -Force .git (rm -r .git)
Remove-item -Recurse -Force .gitignore (rm .gitignore)
cd ..
[COPY MY .gitignore file into your root project dir]
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR_GITLAB_EMAIL@GMAIL.COM"
git init
git add .
git commit -m "Initial commit"
git remote add origin YOUR_GITLAB_PROJECT_HTTPS_URL
git push -u origin master

-----

ssh root@X.X.X.X
apt update

https://github.com/nodesource/distributions

curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
node -v
npm -v

echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list
sudo apt-get update
sudo apt-get install libssl1.1

https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/

sudo apt-get install gnupg curl
curl -fsSL https://pgp.mongodb.com/server-7.0.asc | \
   sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.com/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

sudo systemctl start mongod
sudo systemctl status mongod
sudo systemctl enable mongod

https://pm2.io/

git clone https://gitlab.com/rahulat4200/rststore-19122023.git
ls
cd rststore-19122023
ls
cd frontend
npm install
npm run build
cd ..
npm install

nano .env
[Ctrl v]
[Ctrl o]
[ENTER]
[Ctrl x]

pm2 start backend/server.js
npm run data:import

------------------------------

git add .
git commit -m "Type your message"
git push

ssh into your VM
cd into your folder
git pull
cd frontend
npm run build
cd ..
pm2 restart 0

pm2 status -> check your id

------------------------------

RSTStore
2nd project

Portfolio site =>
-> about yourself
-> skills (MERN, js, html, css, react, redux, mongoDB, nodeJS, express)
-> links (linked in, discord, stackoverflow, gitlab/github)
-> portfolio links (live projects, gitlab repo urls)
-> download resume (pdf)

(vercel.com, netlify.com, render.com)

-------------------------------------------------
DevOps

Data Structures & Algo
Software Arch

Cloud -> AWS/Azure
-------------------------------------------------

