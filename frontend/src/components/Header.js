import {
	Box,
	Button,
	Flex,
	Heading,
	Icon,
	Link,
	Menu,
	MenuButton,
	MenuItem,
	MenuList,
} from '@chakra-ui/react';
import { useState } from 'react';
import { HiMenuAlt3, HiShoppingBag, HiUser } from 'react-icons/hi';
import { IoChevronDown } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { logout } from '../actions/userActions';
import HeaderMenuItem from './HeaderMenuItem';

const Header = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const [show, setShow] = useState(false);

	const userLogin = useSelector((state) => state.userLogin);
	const { userInfo } = userLogin;

	const logoutHandler = () => {
		dispatch(logout());
		navigate('/login');
	};

	return (
		<Flex
			as='header'
			alignItems='center'
			justifyContent='space-between'
			wrap='wrap'
			p='6'
			bgColor='gray.800'
			pos='fixed'
			w='full'
			top='0'
			left='0'
			zIndex='99999'>
			<Link as={RouterLink} to='/'>
				<Heading
					as='h1'
					color='whiteAlpha.800'
					fontWeight='bold'
					size='md'
					letterSpacing='wide'>
					RST Store
				</Heading>
			</Link>

			<Box
				display={{ base: 'block', md: 'none' }}
				pos='relative'
				top='1'
				onClick={() => setShow(!show)}>
				<Icon as={HiMenuAlt3} color='white' w='6' h='6' />
			</Box>

			<Box
				as='nav'
				display={{ base: show ? 'block' : 'none', md: 'flex' }}
				width={{ base: 'full', md: 'auto' }}
				mt={{ base: '3', md: '0' }}>
				<HeaderMenuItem icon={HiShoppingBag} label='Cart' url='/cart' />

				{userInfo ? (
					<Menu>
						<MenuButton as={Button} rightIcon={<IoChevronDown />}>
							{userInfo.name}
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/profile'>
								Profile
							</MenuItem>
							<MenuItem onClick={logoutHandler}>Logout</MenuItem>
						</MenuList>
					</Menu>
				) : (
					<HeaderMenuItem icon={HiUser} label='Login' url='/login' />
				)}
			</Box>
		</Flex>
	);
};

export default Header;
